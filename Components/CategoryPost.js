import React from 'react';
import axios from 'axios';
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faComment} from '@fortawesome/free-solid-svg-icons';
library.add
(
    faComment
);
import {
    Text,FlatList,View,StyleSheet,Linking,Image
  } from 'react-native';
  import {
    List,ListItem
  } from 'react-native-elements';
export default class CategoryPost extends React.Component
{
    state=
    {
        postsreddit:[]
    }
    componentDidMount()
    {
        axios.get('https://www.reddit.com/r/chile/.json').then(res=>{
            console.log(JSON.stringify(res.data.data.children[0].data.title))
         //   const posts=res.data.data.children[0].data;
          const posts=res.data.data.children;
         // console.log(JSON.stringify(posts));
            this.setState({postsreddit:posts});
          
        })
    }
    render()
    {
        return(
         <View style={styles.container}>
           <FlatList
                data={this.state.postsreddit}
                renderItem={({item})=>( 
               <View style={styles.mainRowMain}>
                    <View style={styles.mainRow}>   
                        <View style={{flex:1}}>
                        <Image style={{width:75,height:75}} source={{uri:item.data.thumbnail}}/>
                            </View>               
                   <View style={styles.flatview}>
                       <Text style={styles.author}>{`${item.data.subreddit_name_prefixed} • ${item.data.author} • ${item.data.link_flair_text}`}</Text>
                       <Text style={styles.topics}>{item.data.title}</Text>
                       <Text style={{color:'blue',flex:1}} onPress={()=> Linking.openURL(item.data.url)}>{item.data.domain}</Text>
                   </View>
                      
                   </View>
                   <View><FontAwesomeIcon icon={faComment}/></View>
            </View>
                )}
             />   
         </View>
            
           
        );
    }
}
const styles = StyleSheet.create({
    container: {
 //   flex:3,
      marginTop: 50,
      justifyContent: 'center',
   //   alignItems: 'center',
      backgroundColor: '#F5FCFF'
    },
    h2text: {
      marginTop: 10,
      fontFamily: 'Helvetica',
      fontSize: 36,
      fontWeight: 'bold',
    },
    mainRow: {
        flex:2,
        flexDirection:'row',
       // borderBottomColor: '#d4d2cd',
      //  borderBottomWidth: 1,
       // paddingBottom: 10,
     //   maxHeight:300
      },
      mainRowMain: {
       // flex:2,
        flexDirection:'column',
        borderBottomColor: '#d4d2cd',
        borderBottomWidth: 1,
        paddingBottom: 10,
     //   maxHeight:300
      },
    flatview: {
      justifyContent: 'center',
      paddingTop: 10,
      borderRadius: 2,
      paddingLeft:5,
      flex:3,
      marginRight:20
    },
    flatviewBottom: {
        justifyContent: 'center',
        paddingTop: 5,
        borderRadius: 2,
        paddingLeft:5,
        flex:3,
        marginRight:20
      },
    topics: {
      fontFamily: 'Arial',
      fontSize: 18,
      color:'#222222',
      //flex:3
    },
    author: {
        fontFamily: 'Arial',
        fontSize: 12,
        color:'#222222',
    }
    
  });