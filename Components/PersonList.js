import React from 'react';
import axios from 'axios';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
  } from 'react-native';
export default class PersonList extends React.Component
{
 state={
     persons:[]
 }
 componentDidMount(){
     axios.get('https://jsonplaceholder.typicode.com/users').then(res=>{
         const persons=res.data;
         //console.log(res);
         this.setState({persons});
     })
 }
 render()
 {
     return (
     <Text>
     {this.state.persons.map(person=>person.name)}
     </Text>
     )
 }
}